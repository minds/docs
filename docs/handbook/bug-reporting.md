---
id: bug-reporting
title: Bug reporting
---

## Reporting a bug

The below document outlines the Minds bug reporting process.

### Community members

Community members can either [create a new issue](https://gitlab.com/minds/minds/issues/new), or create a [Support request](https://support.minds.com/hc/en-us/requests/new).

If you would like to to create your own card, please [search existing issues](https://gitlab.com/groups/minds/-/issues?scope=all&utf8=%E2%9C%93&state=opened) to confirm that it is not already in the system, if it is, comment on the existing issue and let us know you are also experiencing it. If there is no matching issue, please create one using the `Bug` template. Once completed just tag a member of the team so that we can properly label your issue.

### Team members

Upon discovering or having an issue reported to you, please refer to the below flowchart:

![Bug Reporting Flow](../assets/bug-reporting-flow.png "Diagram of the Minds Bug Reporting Flow")
[Click to enlarge](../assets/bug-reporting-flow.png)

If you believe the issue is severe enough to warrant the triggering of Superhero, take a look at [the Superhero page](https://developers.minds.com/docs/handbook/superhero/). Urgent bugs are lesser than superhero, but still need attention from a developer as soon as possible. Keep in mind that urgent issues disrupt the current plan for the sprint and require immediate attention from a developer.
