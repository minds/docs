---
id: dataoverview
title: Data ETL Pipeline Overview
---
# Data Pipeline Overview

The main components of the Minds ETL data pipeline are as follows:

- **Data sources**: these are sources of event and entity state data from various internal (e.g. Snowplow app events) and external (e.g. Stripe transactions)
- **Data sinks**: these are destinations for processed data and include the Snowflake data warehouse, the Vitess application database, the Elastic search engine, and various Pulsar pub/sub event streams
- **Extract/Transform/Load (ETL) scripts**: these are scripts usually written in Python, which usually run under the [Prefect](https://www.prefect.io/) orchestration manager, but also may run as standalone scripts running in containers in the production infrastructure (e.g. [Img To Caption](https://gitlab.com/minds/data-science/img-to-caption)), or as [Airbyte](https://airbyte.com/) connectors
- **Data transformation scripts**: these are [dbt](https://www.getdbt.com/) scripts which transform the unprocessed data loaded from various sources into the Snowflake data warehouse into a comprehensive data model useful for business analytics and product functionality

## Repositories

### [minds-etl](https://gitlab.com/minds/data-science/minds-etl)

This is the main repository for the ETL pipeline. It consists of three main sub-directories: one for creating the run-time docker containers for the Prefect and environment, one for the dbt transformation models, and one for the "[flows](https://docs.prefect.io/2.11.5/concepts/flows/)", which are the orchestrated python scripts that perfom extract, transform and load functions.

The repository structure is as follows:
- minds-etl
  - dbt _(dbt transformation models)_
    - dbt_project.yml _(configuration of the overall dbt project)_
    - macros _(miscellaneous dbt support macros)_
    - models _(the dbt data transformation models)_
      - sources _(specification of external data sources used by models in the dbt project)_
      - staging _(models dealing with initial ingestion/preprocessing of source data)_
      - checkpoint _(models that set high-water marks for processed data between runs to enable incremental processing )
      - intermediate _(models that perform miscellaneous involved transformations between staging and domain models)_
      - domain _(models that consolidate data from various sources into a domain-specific entity-relation model)_
      - kpi _(models that peform various statistical aggregations over the domain models, which can be used in dashboards or presentation models)_
      - presentation _(models intended to show a particular view of the domain model and or kpi statistics, for use in data visualisation tools such as superset)_
  - docker _(configuration and setup of the run-time docker containers for the Prefect and Airbyte environments)_
    - docker-compose.yml _(very important file which defines the configuration and orchestration of the different docker containers used by Prefect and Airbyte)_
    - prefect _(configuration, setup, and dependencies for the Prefect "[agent](https://docs.prefect.io/2.11.5/concepts/agents/)" container)_
      - setup.sh _(initialization shell script for the Prefect agent container)_
      - requirements.txt _(python dependencies for the prefect "flow" scripts, including libraries needed to connect to various data sources/sinks, etc.)_
      - config.toml _(setup configs for Prefect operating mode: server vs cloud)_
      - mautic-0.1.0-py38-none-any.whl _(python library dependency to connect to Mautic)_
    - docker-entrypoint-initdb.d
      - docker-initialize-multiple-databases.sh _(helper script to initialize a single Postgresql with multiple application databases, e.g. Prefect and Airbyte)_
  - prefect _(directory containing all the various ETL scripts ("flows") which are managed by the Prefect environment)_

### [etl](https://gitlab.com/minds/data-science/etl)

This is a public-facing cleaned-up version of minds-etl, with commit history curated to make the code easier to follow.

### [img-to-caption](https://gitlab.com/minds/data-science/img-to-caption)

This is a standalone docker-compose environment that generates captions from images posted to minds, and generates topic category tags from the activity text and generated captions. The main script is [caption-images.py](https://gitlab.com/minds/data-science/img-to-caption/-/blob/main/caption-images.py) which performs the following operations:

- Listen to the `entities-ops` pulsar stream
- When a new activity is identified on the stream, it is checked for image or video attachments
- If there are image attachments, they are loaded from the CDN, if there is a video attachement the thumbnail is loaded from the CDN
- A [BLIP-2](https://huggingface.co/docs/transformers/main/model_doc/blip-2) transformer model is used to generate a caption for each of the image(s)
- A record for the activity and a record for each caption is published to the `captioned-activities` pulsar stream
- The body text, tags, and the captions for an activity are consolidated into a single text summary, which is fed to a trained LLM to generate a list of topical category tags
- A record for the activity including the text summary and list of topical category tags are published to the `inferred-tags` pulsar stream

The `/terraform` directory contains the docker-compose and terraform configs and setup scripts for provisioning a server to run the `caption-images.py` script in production. Because of the AI neural models used by this script it must be specially provisioned on a GPU-accellerated server instance with the appropriate hardware specifications.

