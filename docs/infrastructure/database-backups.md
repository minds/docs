---
id: db-backup
title: Databases Backup and Restore
---

## Databases Backup and Restore

We make use of several different databases with different procedures for taking and restoring backups.

***NOTE***: In order to access a database running in Kubernetes, you can use port-forwarding with `kubectl`. For example:

```bash
kubectl get svc -n "${NAMESPACE}" # Find the K8s service name
kubectl port-forward "svc/${SERVICE}" -n "${NAMESPACE}" "${LOCALHOST_PORT}:${REMOTE_PORT}"
```

### Vitess

#### Backup

See also the [official documentation](https://vitess.io/docs/16.0/reference/programs/vtctldclient/vtctldclient_backupshard/) for this command.

Autmated backups are taken periodically for both Minds and Mautic keyspaces.

Vitess backups are stored in Oracle OSS, and can be initiated using `vtctlclient`:

```bash
vtctldclient --server "${VTCTLD_SERVER}" BackupShard "${KEYSPACE}/${SHARD}"
```

Note that when taking a backup for Mautic, you will need to use the `--allow-primary` flag. This is due to the fact that Mautic currently uses a single tablet.

```bash
vtctldclient --server "${VTCTLD_SERVER}" BackupShard --allow-primary "mautic/-"
```

#### Restore

See also the [official documentation](https://vitess.io/docs/16.0/reference/programs/vtctldclient/vtctldclient_restorefrombackup/) for this command.

New tablets will automatically be restored from the latest backup. In order to manually restore a tablet using a specific backup, use `vtctldclient`:

```bash
DATE=YYYY-mm-DD.HHMMSS vtctldclient --server "${VTCTLD_SERVER}" RestoreFromBackup --backup-timestamp "${DATE}" "${TABLET_ALIAS}"
```

### Postgres

See also the [official documentation](https://www.postgresql.org/docs/current/backup-dump.html) for Postgres backups.

Like Vitess, most Postgres databases will have automated backups taken periodically. In order to backup manually, you can use `pg_dump` or `pg_dumpall`.

```bash
pg_dump -C -h "${DB_HOST}" -p "${DB_PORT}" -d "${DATABASE}" > backup.sql

# Or

pg_dumpall -C -h "${DB_HOST}" -p "${DB_PORT}" > backup_all.sql
```

Then, you can restore the dump using psql:

```bash
psql -h "${DB_HOST}" -p "${DB_PORT}" < backup.sql
```

### MongoDB

#### Backup

See also the [official documentation](https://www.mongodb.com/docs/manual/tutorial/backup-and-restore-tools) for these commands.

To manually take a MongoDB backup using `mongodump`:

```
mongodump \
   --host=${DATABASE_HOST} \
   --port=${DATABSE_PORT} \
   --username=${USER} \
   --password=${PASSWORD} \
   --out=/tmp/mongo-backup
```

#### Restore

```
mongorestore \
   --host=${DATABASE_HOST} \
   --port=${DATABSE_PORT} \
   --username=${USER} \
   --password=${PASSWORD} \
   --authenticationDatabase=admin \
   /tmp/mongo-backup
```
