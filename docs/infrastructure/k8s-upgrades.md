---
id: k8s-upgrades
title: Kubernetes Upgrades
---

## Kubernetes Upgrades

### OKE

***NOTE***: First read the official Oracle Docs for OKE version upgrades [here](https://docs.oracle.com/en-us/iaas/Content/ContEng/Concepts/contengaboutupgradingclusters.htm).

When upgrading clusters, you will first need to check for compatibility with all other infrastructure modules. Modules that integrate with Kubernetes directly such as Vault and cert-manager should receive the most attention.

[kubepug](https://github.com/kubepug/kubepug) is a helpful utility for checking clusters for objects that will be deprecated with new versions of K8s.

After verifying compatibility, upgrade the control plane first followed by the worker nodes. Nodes will need to be cycled manually after an upgrade as we do not use managed node groups. Both the control plane and the worker groups can be upgraded using the console or CLI. After an upgrade, make sure to update the cluster version within the Terraform code.
