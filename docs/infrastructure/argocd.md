---
id: db-backup
title: Databases Backup and Restore
---

## Argo CD

We deploy most Helm charts through Argo CD, which can be logged into using SSO credentials. You must be on the VPN in order to access Argo CD, and you must also have the appropriate permissions (DevOps or Superhero roles) in Keycloak to deploy changes.

### App of Apps Repo

In order to deploy a Helm chart using Argo CD, you must add an application to [this](https://gitlab.com/minds/infrastructure/minds-argocd-applications) repository. View this project's readme for more details. After adding the manifest for your application, sync the "apps" app within Argo CD in order to create the application.

### Argo CD Helm Chart

The Helm chart for Argo CD is [here](https://gitlab.com/minds/infrastructure/modules/argo-cd). This will need to be deployed manually with `helm` by someone with Kubernetes admin privileges into the `argo-cd` namespace.

### Import/Export

In order to migrate Argo CD to another cluster (or for taking a backup of all Argo CD state), you can use the builtin import/export functionality within the `argocd` cli. See [this](https://argo-cd.readthedocs.io/en/stable/operator-manual/disaster_recovery/) doc for details.
