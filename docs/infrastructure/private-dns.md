---
id: private-dns
title: Private DNS
---

# Private DNS

## Trust CA

In order to connect to internal Minds services using the Firezone VPN, you will need to first configure your system to trust our private Certificate Authority (CA). The steps are slighly different depending on which operating system and web brower you are using. In many cases, you can probably get away with only trusting the CA in your browser.

***NOTE***: First off, you will need to download the public key for our root CA from Bitwarden. It will be labeled as "Minds Private CA (Public key)".

### Web Browsers

Most browsers use their own truststore rather than the system's. First, let's configure our browser to trust our CA.

#### Firefox

1. Click on the pancake menu in the top right, and click on `Settings`.
2. Click on `Privacy and Security` in the left menu, then scroll down to `Certificates` in the `Security` section.
3. Click on `View certificates`, and click on the `Authorities` tab in the top bar.
4. Click on `Import...` and navigate to the PEM file you downloaded previously.
5. Import the CA.

That's all! Now Firefox should recognize the certs for our private services as valid.

***NOTE***: In order to resolve private hostnames when connected to the VPN, you will need to configure Firefox to use your system DNS rather than CloudFlare's DNS over HTTPS (DoH) provider.

1. Click on the pancake menu in the top right, and click on `Settings`.
2. Click on `Privacy and Security` in the left menu, then scroll down to `DNS over HTTPS`.
3. The simplest solution is to simply disable DoH by clicking `Off`. 

***NOTE***: If you want to use DoH where possible you can also add exceptions for each private Minds service that you want to consume.

#### Chromium (Google Chrome, Brave, Opera)

1. Click on the pancake menu in the top right, and click on `Settings`.
2. Click on `Privacy and security` in the left menu, then scroll down and click on `Manage certificates`.
3. Click on the `Authorities` tab in the top bar.
4. Click on `Import` and navigate to the PEM file you downloaded previously.
5. Import the CA.

That's all! Now this Chromium-based browser should recognize the certs for our private services as valid.

### Operating Systems

#### Ubuntu

***Note***: Tested on Ubuntu 22.04, the process may be different for your Linux distribution.

1. Copy the PEM file you downloaded to the `/usr/local/share/ca-certificates` folder.
    - ***NOTE***: Make sure to use a `.crt` file extension. For example:
        `cp minds-private-ca.pem /usr/local/share/ca-certificates/minds-private-ca.crt`
2. Run `sudo update-ca-certificates`.
3. Test connecting to a private service with `curl` or some other tool.

#### macOS

1. Click on the certificate.
2. Select the `System` keychain, and click `Ok`.
3. Test connecting to a private service with `curl` or some other tool.
