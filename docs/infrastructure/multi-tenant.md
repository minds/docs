## Table

owner_guid is the Minds (host) owner guid.

root_user_guid is the default admin of the site. Assets will be stored under this user and be the root admin.

## Votes

Votes contain a `tenant_id` index. **NULL** value is the host site.

We do a nasty hack in MySQLRepository to get the votes counts and pass it through to the Activity entity. This doesn't seem ideal and should be done at the 'edge'. That can be refactored in the future with GraphQL.

