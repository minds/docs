---
id: terraform
title: Terraform
---

## Terraform

We make use of Terraform for deploying and managing cloud infrastructure.

### Shared Infrastructure Repo

The "main" Terraform repository contains the relevant code for networking and compute. This can be found [here](https://gitlab.com/minds/infrastructure/minds-terraform).

The repo has the following structure:

```bash
.
├── legacy  # Legacy Terraform Code
├── live    # Terragrunt Workspaces
│   ├── aws
│   ├── bridge
│   └── oci
│       └── minds-oci-kubernetes
│           ├── ops
│           ├── production
│           └── sandbox
└── modules # Modules for shared infrastructure
    ├── aws
    ├── bridge
    └── oci
```

As this repo uses `terragrunt`, you will need to deploy changes to the Kubernetes module using the `live/oci/minds-oci-kubernetes/$ENVIRONMENT` folder:

```bash
ENVIRONMENT="sandbox"

cd live/oci/minds-oci-kubernetes/$ENVIRONMENT
terragrunt init
terragrunt plan
terragrunt apply
```

### Individual Modules

Most Terraform will not be in the shared repo, but in the individual module repositories. For example, the Terraform code for Mautic is [here](https://gitlab.com/minds/infrastructure/modules/mautic/-/tree/master/terraform). Usually this will be in a `terraform` subfolder for the module.

These modules typically do not use `terragrunt`, but instead are deployed with `terraform` directly:

```bash
cd terraform
terraform init
terraform plan
terraform apply
```