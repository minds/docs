---
id: cloud-access
title: Cloud Access
---

## Oracle Cloud (OCI)

### Setting up the CLI

The OCI CLI is required for using Kubernetes (OKE). [Click here](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm) for more information about how to install.

### Authentication

You should use the 'security token' authentication _([more info](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/clitoken.htm))_.

```
export OCI_CLI_AUTH=security_token
```

A session lasts for 1 hour, you can authenticate by running:

```
oci session authenticate --profile-name DEFAULT --region us-ashburn-1
```

#### Tenancy name

`mindsoci`

#### Sign in method

Under _'Or sign in with'_, select **keycloak**.

### Kubernetes (OKE)

#### Kubeconfig

##### Sandboxes

```
oci ce cluster create-kubeconfig --cluster-id ocid1.cluster.oc1.iad.aaaaaaaa2mehpgv7kggpaxqfm54r6wsbbb7snp2hswbwcsa43cyiarjo4tsa --file $HOME/.kube/config --region us-ashburn-1 --token-version 2.0.0  --kube-endpoint PUBLIC_ENDPOINT
```


##### Production

```
oci ce cluster create-kubeconfig --cluster-id ocid1.cluster.oc1.iad.aaaaaaaappzbfmfsxguuqnwk2xtqqishajefesakh7h4lonivc2nnvhcgaha --file $HOME/.kube/config --region us-ashburn-1 --token-version 2.0.0  --kube-endpoint PUBLIC_ENDPOINT
```

#### Kubectl

After setting up your Kubeconfig, above, you can they use `kubectl` as usual. 