---
id: observability
title: Observability
---

## Application Monitoring

**Tools**:

- [Sentry](https://sentry.io/for/performance/)

## Dashboards

**Tools**:

- [OpenSearch](https://opensearch.org/)
- [Grafana](https://grafana.minds.com)

- [Superset](https://superset.minds.com)

These are used to visualize infrastructure metrics, logs, and traces. Grafana should be considered the "single pane of glass" in most cases, and this is the preferred destination for dashboards. OpenSearch will be used for logging dashboards specifically.

## Metrics

**Tools**:

- [Prometheus](https://prometheus.io/docs/introduction/overview/)
- [Prometheus Client Libraries](https://prometheus.io/docs/instrumenting/clientlibs/)

**Secondary Tools**:

- [CloudWatch](https://aws.amazon.com/cloudwatch/)
- [OCI Monitoring](https://docs.oracle.com/en-us/iaas/Content/Monitoring/Concepts/monitoringoverview.htm)

Prometheus should be the preferred destination for application and infrastructure metrics.

## Logging

**Tools**:

- [fluentbit](https://fluentbit.io/)
- [fluentd](https://opensearch.org/)

These tools allow for retention and historical analysis of server logs, as well as alerting on specific log messages. Currently we do not have OpenSearch dashboards available for use, although this is in progress.

## Tracing

**Tools**:

- [Jaeger](https://www.jaegertracing.io/)
- [Kiali](https://kiali.io/)
- [Istio](https://istio.io/latest/)

Distributed tracing allows for monitoring the flow of network requests and performance and latency monitoring. Kiali and Jaeger are currently not in place on OKE yet.
