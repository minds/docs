---
id: monitoring
title: Monitoring
---

## Monitoring

We use a variety of tools for monitoring and observability. This page documents which tools are used for what purposes.

### Sentry

Sentry is used for application level alerts and monitoring. You can login to Sentry [here](https://sentry.io/auth/login/) using your SSO account.

### Prometheus

Prometheus is used to monitor most infrastructure components running in Kubernetes. We deploy and manage it using the [Prometheus Operator](https://github.com/prometheus-operator/prometheus-operator).

Prometheus resources exist on the `stateful-monitoring` node group, and are deployed within the `monitoring` namespace. You can access Prometheus through port-forwarding, although this is rarely necessary.

### Grafana

Grafana can be accessed [here](https://grafana.minds.com). You can login with your SSO account for read only access, but then will need to ask an admin for editor access in order to update dashboards.

Grafana is deployed on the stateless node groups and is deployed into the `grafana` namespace. Grafana uses the `sqlite` backend.
