---
id: nostr
title: Nostr
---

Nostr is a protocol, designed for simplicity, that aims to create a censorship-resistant global social network. Let's unpack that a little:

**Simple**

The protocol is based on very simple & flexible event objects (which are passed around as plain JSON) and uses standard elliptic-curve cryptography for keys and signing. The only supported transport is websockets connections from clients to relays. This makes it easy to write clients and relays and promotes software diversity.

**Resilient**

Because Nostr doesn't rely on a small number of trusted servers for moving or storing data, it's very resilient. The protocol assumes that relays will disappear and allows users to connect and publish to an arbitrary number of relays that they can change over time.

**Verifiable**

Because Nostr accounts are based on public-key cryptography it's easy to verify messages were really sent by the user in question.

Like HTTP or TCP-IP, Nostr is a protocol; an open standard upon which anyone can build. Nostr is not an app or service that you sign up for.

## Nostr NIPs Compliance

Nostr Implementation Possibilities (NIPs) exist to document what may be implemented by Nostr-compatible relay and client software. [NIP-26](https://github.com/nostr-protocol/nips/blob/master/26.md) was introduced in a collaborative effort between Minds and the Nostr community to create a model for allowing delegated identities to produce valid events on behalf of a user.

For more information on what NIPs are available as well as the details of each NIP, see [this document](https://github.com/nostr-protocol/nips).

✅ - Implemented

🚧 - In Progress / Partially Implemented

❌ - Not (Yet) Implemented

| NIP      | Status |
| -------- | ------ |
| NIP-01   | ✅     |
| NIP-02   | ❌     |
| NIP-03   | ❌     |
| NIP-04   | ❌     |
| NIP-05   | 🚧     |
| NIP-06   | ❌     |
| NIP-07   | ❌     |
| NIP-08   | ✅     |
| NIP-09   | ✅     |
| NIP-10   | ✅     |
| NIP-11   | ❌     |
| NIP-12   | ❌     |
| NIP-13   | ❌     |
| NIP-14   | ❌     |
| NIP-15   | ❌     |
| NIP-16   | ❌     |
| NIP-26   | 🚧     |

## FAQ

### How do I ensure my Minds posts are being served to Nostr clients?

In order to view your Minds posts using a Nostr client, you must configure NIP-26 delegation on your Minds account.

### How do I setup delegation on my Minds account?

First, go to the "Settings" page for your account:

![Settings page](../assets/nostr/settings.png)

Next, click on "Nostr":

![Settings page 2](../assets/nostr/settings-2.png)

Finally, you can either provide your own keys or generate new ones by clicking on the highlighted text. Then, click "Setup" to complete:

![Delegation page](../assets/nostr/delegation.png)

### How do I view my posts using a Nostr client?

First, you will need to pick a Nostr client. There are [many options](https://github.com/aljazceru/awesome-nostr) to choose from, however we'll use Snort as an example.

Go to [this page](https://snort.social/login) and either provide your existing login or create an account.

After creating an account or logging into an existing one, go to [this page](https://snort.social/settings/relays) in order to view your configured relays.

Under the "Add Relays" section, input the Minds relay: `wss://relay.minds.com/nostr/v1/ws` and click "Add".

![Snort relays](../assets/nostr/snort.png)

Make sure to disable the "Write" option as we only support reading events currently:

![Snort Minds relay](../assets/nostr/snort-relay.png)

Now you should be able to lookup your Minds username using the format of: `<username>@minds.com`

![Snort lookup](../assets/nostr/snort-lookup.png)

For example, here is the main Minds account:

![Snort profile](../assets/nostr/snort-profile.png)

If you can see your posts, they should be syncing. Any updates you make on Minds should recreate the events.
