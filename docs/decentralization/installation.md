---
id: activitypub
title: ActivityPub
---

Minds can interoperate (federate) with other networks that implement the [ActivityPub](https://www.w3.org/TR/activitypub/) protocol, allowing members to communicate with each other; much like email, where you can mail other providers, so long as you know their email address.

## Spec compliance 

### Activities

> ↗️ [ActivityFactory.php](https://gitlab.com/minds/engine/-/blob/master/Core/ActivityPub/Factories/ActivityFactory.php?ref_type=heads#L80)

| Activity | Supported on | Notes |
| :--- | :--- |  :--- | 
| Create | [Note](#note) ✅ | Creates a post and saves to the database |
| Announce | [Note](#note) ✅ | Creates a remind and saves to the database |
| Like | [Note](#note) ✅ | Creates an Up Vote on a post | 
| Follow | [Person](#person) ✅ | Subscribes to a channel |
| Accept | Follow ✅ | A subscribe request was accepted |
| Delete |  [Note](#note) ⚒️ [Person](#person) ⚒️ | https://gitlab.com/minds/engine/-/issues/2630 |
| Undo | Like ✅ Announce ✅ Follow ✅ | Undo a remind, subscription or up vote |
| Flag | [Note](#note) ⚒️ Actor ⚒️ | https://gitlab.com/minds/engine/-/issues/2617 |
| Block | [Person](#person) ⚒️ | |
| Move | [Person](#person) ❌ | |

### Objects

> ↗️ [ObjectFactory.php](https://gitlab.com/minds/engine/-/blob/master/Core/ActivityPub/Factories/ObjectFactory.php?ref_type=heads#L228)

| Object | Notes |
| :--- | :--- | 
| [Note](#note) | Comments currently only support a single image attachment |
| [Image](#imagedocument)  |  An extension of the Document object |
| [Document](#imagedocument) | A more generic interface to use for multiple different type of attachment  | 

#### Note

- A minds 'activity' is transformed into a Note.
- Quote posts are transformed to Note objects and will use the inReplyTo property to reference the source post
- Comments are also transformed to a Note and will also use the inReplyTo property
- All ingested Note objects will be transformed into comments

| Property | Type | Notes |
| :--- |  :--- |  :--- | 
| content | string | Text of the post **(Note: this is represented as HTML)** |
| inReplyTo | string | The id of the post that is being replied to. Comments and Quote posts used this property. |
| published | string | The timestamp of the post date |
| url | string | A url that will link to the original post. Falls back to the `id` field if not provided |
| attributedTo | string | The id of the post owner |
| to/cc | string[] | Specifies the audience (ie. followers) and visibility level of a post |
| attachment | [Image[]\|Document[]](#imagedocument) | Attached media (images). Video support coming soon. |

#### Image/Document

| Property | Type | Notes |
| :--- |  :--- |  :--- | 
| url | string | The source of the attachment |
| mimeType | string | ie. `image/jpeg` |


### Actors

> ↗️ [ActorFactory.php](https://gitlab.com/minds/engine/-/blob/master/Core/ActivityPub/Factories/ActorFactory.php?ref_type=heads#L148)

| Actor | Notes |
| :--- | :--- |  :--- | 
| [Person](#person) | Transformed to a User (channel) |

#### Person

| Property | Type | Notes |
| :--- |  :--- |  :--- | 
| preferredUsername | string | Used for a username. Must be unique per domain and correspond to a Webfungert `acct:` URI |
| name | string | A display name for the user |
| summary | string | A brief description or 'bio' on the users channel page |
| url | string | A url that will link to the original profile. Falls back to the `id` field if not provided |
| icon | string | The channels avatar |
| manuallyApprovesFollowers | boolean | Always false |
| publicKey | PublicKey | The public key of the actor. Network requests from this actor will be signed with their respective private key |

## Background runners

Events are emitted via background runners to improve performance and durability. At this time, Minds will not attempt to redeliver failed events, but we will start supporting retries in the future.

| Purpose | File | Script |
| :--- |  :--- |  :--- | 
| Dispatches new posts and deletes | [ActivityPubEntitiesOpsSubscription.php](https://gitlab.com/minds/engine/-/blob/master/Core/ActivityPub/Subscriptions/ActivityPubEntitiesOpsSubscription.php?ref_type=heads) | `php cli.php EventStreams --subscription=Core\\ActivityPub\\Subscriptions\\ActivityPubEntitiesOpsSubscription` |
| Dispatches subscribe and up vote events | [ActivityPubEventStreamsSubscription.php](https://gitlab.com/minds/engine/-/blob/master/Core/ActivityPub/Subscriptions/ActivityPubEventStreamsSubscription.php?ref_type=heads) | `php cli.php EventStreams --subscription=Core\\ActivityPub\\Subscriptions\\ActivityPubEntitiesOpActivityPubEventStreamsSubscriptionsSubscription` |#

## FAQ

### Why are my posts not showing up on Minds?

You must be subscribed to (followed by) at least one user for your posts to federate with Minds. 

### Why are my posts not showing up on other networks?

Other networks have different criteria for accepting federated posts. We recommend that at least one user on the other network subscribes to you for your posts to be shown. ([See mastodon criteria](https://github.com/mastodon/mastodon/blob/ae6cf33321a9f240ef73666a552e552b65390012/app/lib/activitypub/activity/create.rb#L389C7-L389C32)).

### Why can I not see all posts from a federated user?

Minds will only display posts from the moment they are subscribed to onwards. Similarly, if Minds has not be notified of a new post, it will also not display as we do not poll users outboxes.

### How can I follow someone who is not on Minds?

We are working to make this easier ([engine#2628](https://gitlab.com/minds/engine/-/issues/2628)), however for now you can follow a channel on another network by putting their username in the address bar (ie. https://www.minds.com/minds@mastodon.social).