---
id: strapi
title: Strapi
---

## What is Strapi

[Strapi](https://strapi.io/) is an open source headless CMS that we use at Minds as a content API. It enables us to make quick and simple changes through our self-hosted Strapi UI to areas where changes can frequently occur. For more information check [their docs](https://docs.strapi.io/).

## Setup

### Engine configuration

The Minds development enviroment can be configured to point at either sandbox or local environments by adding one of the below to your engine's `settings.php`.

```php
// Sandbox
$CONFIG->set('strapi', [
    'url' => 'https://cms.oke.minds.io'
]);

// Local
$CONFIG->set('strapi', [
    'url' => 'http://localhost:1337'
]);
```

### Setting up Strapi

To set up Strapi locally, first you need to clone the [Minds Strapi repository](https://gitlab.com/minds/strapi).

```sh
# SSH
git clone git@gitlab.com:minds/strapi.git

# HTTPS
git clone https://gitlab.com/minds/strapi.git
```

Next we will install Strapi via yarn (note: NPM can also be used but is slower for this purpose).

```sh
cd strapi
yarn
```

From here you need to copy over the `.env.example` file to an actual `.env` file.

```sh
cp .env.example .env
```

Next, fill out the `.env` file by adding in randomly generated 16 byte base64 strings to all areas that have the text: `tobemodified`.

For a development environment you could search for a web app to do this quickly as security is not as critical, but for any customer facing / production deployments you should generate them offline yourself. To generate these offline yourself in a node environment for example, you could run `crypto.randomBytes(16).toString('base64')`.

### Running Strapi as a dev server

From here you can run the following to start your dev server:

```sh
yarn develop
```

Once it is running, navigate to `localhost:1337` as you should see the Strapi UI, prompting you to create the initial admin account.
