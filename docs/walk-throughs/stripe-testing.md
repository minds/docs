---
id: stripe-testing
title: Stripe Testing
---

# Stripe Testing

### Important!

- **Do NOT change an existing users email address to a stripe test one - make a new user or you risk permanently disabling the accounts monetary capabilities.**
- **Do NOT send huge Boosts via test-mode - they WILL eat up views in the pool - ideally reject any Boosts you make**.
- **Do NOT send other transaction types to non-test-mode users - this would be very confusing for them**.
- This is only possible for team members.

### Setting up an account with testmode.

To use Stripe in test-mode, first you will need access to a `minds.com` email address. You need to make a new account with an email address that has `+stripetest` before the `@` sign and after your username. For example, given my email is `dave@minds.com`, I enter my email as `dave+stripetest@minds.com`. Complete registration by verifying that email address.

### Adding a card

To add a card, go through a normal add card flow (e.g. try to Boost a post or send a Supermind), but when inputting your card, [use a Stripe test card](https://stripe.com/docs/testing#cards).

### Add a cash account to receive 

You can set up a cash account to receive funds in test mode also - but note that you will only be able to receive payments from _OTHER_ testmode accounts. You can do this by going through the normal Stripe account setup flow in the Cash section of your wallet.
