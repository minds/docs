---
id: vitess-schema-changes
title: Vitess Schema Changes
---

# Vitess Schema Changes

## Add or Remove Columns

***NOTE***: Please read [this](https://docs.percona.com/percona-xtrabackup/8.0/error-message-instant.html) article for more information.

When adding or removing columns from a table in Vitess, we should ensure that we specify `ALGORITHM=INPLACE` in the query. This is because Percona xtrabackup does not support the default `ALGORITHM=INSTANT`.

Example:

```sql
ALTER TABLE …, ALGORITHM=INSTANT;
```

You can also confirm that no row versions have been created with this query:

```sql
SELECT NAME FROM INFORMATION_SCHEMA.INNODB_TABLES WHERE TOTAL_ROW_VERSIONS > 0;
```

If this returns any results, you should run `OPTIMIZE TABLE` on the affected tables in order to prevent backup failures.
